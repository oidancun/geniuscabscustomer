package ke.co.eloise.geniuscabs;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class EndScreenActivity extends AppCompatActivity {

    private TextView mPrice, mBasePrice, mDistancePrice, mTimePrice;
    private Button mContinue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end_screen);

        mPrice = (TextView) findViewById(R.id.price_textview);
        mBasePrice = (TextView) findViewById(R.id.base_txt);
        mDistancePrice = (TextView) findViewById(R.id.distance_txt);
        mTimePrice = (TextView) findViewById(R.id.time_txt);
        mContinue = findViewById(R.id.cont_btn);
        String price = getIntent().getExtras().getString("priceToPay");
        String basePrice = getIntent().getExtras().getString("priceRideBase");
        String timePrice = getIntent().getExtras().getString("priceRideTime");
        String distancePrice = getIntent().getExtras().getString("priceRideDistance");
        mPrice.setText(price);
        mBasePrice.setText("Base fare: " + basePrice);
        mTimePrice.setText("Time: " + timePrice);
        mDistancePrice.setText("Distance: " + distancePrice);

        mContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EndScreenActivity.this, DriverMapsActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
