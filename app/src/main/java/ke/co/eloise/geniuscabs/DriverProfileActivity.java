package ke.co.eloise.geniuscabs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class DriverProfileActivity extends AppCompatActivity {

    private EditText mEmail, mName, mPhone, mService;
    private Button mConfirm;
    private RadioGroup mRadioGroup;
    private CircleImageView mDriverProfileImage;
    private String mProfileImageUrl, userID;
    private Uri resultUri;

    private FirebaseAuth mAuth;
    private DatabaseReference mDriverDatabase;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_profile);

        mEmail = (EditText) findViewById(R.id.email_et);
        mName = (EditText) findViewById(R.id.fullname_et);
        mPhone = (EditText) findViewById(R.id.phone_et);
        mService = (EditText) findViewById(R.id.service_et);
        mConfirm = (Button) findViewById(R.id.confirm_btn);
        mDriverProfileImage = (CircleImageView) findViewById(R.id.driverProfileImage);
        mAuth = FirebaseAuth.getInstance();
        final String userID = mAuth.getCurrentUser().getUid();
        mDriverDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Riders").child(userID);

        mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        mRadioGroup.check(R.id.taxi);
        mDriverProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });



        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(userID);
        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("name") != null){
                        String userName = map.get("name").toString();
                        mName.setText(userName);
                    }
                    if(map.get("phone") != null){
                        String userPhone = map.get("phone").toString();
                        mPhone.setText(userPhone);
                    }
                    if(map.get("email") != null){
                        String userEmail = map.get("email").toString();
                        mEmail.setText(userEmail);
                    }
                    if(map.get("car") != null){
                        String userService = map.get("car").toString();
                        mService.setText(userService);
                    }
                    if(map.get("service") != null ){
                        if(map.get("service").equals("Boda")){
                            mRadioGroup.check(R.id.boda);
                        }
                        if(map.get("service").equals("Taxi")){
                            mRadioGroup.check(R.id.taxi);
                        }
                        if(map.get("service").equals("Classy")){
                            mRadioGroup.check(R.id.classy);
                        }

                    }
                    if(map.get("profileImageUrl") != null){
                        mProfileImageUrl = map.get("profileImageUrl").toString();
                        Glide.with(getApplication()).load(mProfileImageUrl).into(mDriverProfileImage);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int selectId = mRadioGroup.getCheckedRadioButtonId();
                final RadioButton radioButton = (RadioButton) findViewById(selectId);
                String userName = mName.getText().toString();
                String userEmail = mEmail.getText().toString();
                String userPhone = mPhone.getText().toString();
                String userService = mService.getText().toString();
                String taxiService = radioButton.getText().toString();
                if(radioButton.getText() == null){
                    return;
                }

                Map userInfo = new HashMap();
                userInfo.put("name", userName);
                userInfo.put("email", userEmail);
                userInfo.put("phone", userPhone);
                userInfo.put("car", userService);
                userInfo.put("service", taxiService);
                mDriverDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(userID);
                mDriverDatabase.updateChildren(userInfo);

                if(resultUri != null){
                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("profile_images").child(userID);
                    Bitmap bitmap = null;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    UploadTask uploadTask = filePath.putBytes(data);

                    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();

                            }
                            Toast.makeText(DriverProfileActivity.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                            // Continue with the task to get the download URL
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                Map newImage = new HashMap();
                                newImage.put("profileImageUrl", downloadUri.toString());
                                mDriverDatabase.updateChildren(newImage);
                                finish();
                                return;
                            } else {
                                // Handle failures
                                Toast.makeText(DriverProfileActivity.this, "Image not uploaded", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });




                }else{
                    finish();
                }

                Intent intent = new Intent(DriverProfileActivity.this, DriverDetailsActivity.class);
                startActivity(intent);
                finish();


            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK);
        final Uri imageUri = data.getData();
        resultUri = imageUri;
        mDriverProfileImage.setImageURI(resultUri);
    }

    private void getUserInfo() {

    }
}
