package ke.co.eloise.geniuscabs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomerProfileActivity extends AppCompatActivity {

    private EditText mEmail, mName, mPhone;
    private Button mConfirm;
    private CircleImageView mProfileImage;
    private String mProfileImageUrl;
    private Uri resultUri;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener firebaseAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_profile);

        mEmail = (EditText) findViewById(R.id.email_et);
        mName = (EditText) findViewById(R.id.fullname_et);
        mPhone = (EditText) findViewById(R.id.phone_et);
        mConfirm = (Button) findViewById(R.id.confirm_btn);
        mProfileImage = (CircleImageView) findViewById(R.id.customerProfileImage);

        mProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });


        mAuth = FirebaseAuth.getInstance();
        final String userID = mAuth.getCurrentUser().getUid();
        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID);
        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("name") != null){
                        String userName = map.get("name").toString();
                        mName.setText(userName);
                    }
                    if(map.get("phone") != null){
                        String userPhone = map.get("phone").toString();
                        mPhone.setText(userPhone);
                    }
                    if(map.get("email") != null){
                        String userEmail = map.get("email").toString();
                        mEmail.setText(userEmail);
                    }
                    if(map.get("profileImageUrl") != null){
                        mProfileImageUrl = map.get("profileImageUrl").toString();
                        Glide.with(getApplication()).load(mProfileImageUrl).into(mProfileImage);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String userName = mName.getText().toString();
                String userEmail = mEmail.getText().toString();
                String userPhone = mPhone.getText().toString();

                Map userInfo = new HashMap();
                userInfo.put("name", userName);
                userInfo.put("email", userEmail);
                userInfo.put("phone", userPhone);
                final DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(userID);
                mCustomerDatabase.updateChildren(userInfo);

                if(resultUri != null){
                    final StorageReference filePath = FirebaseStorage.getInstance().getReference().child("profile_images").child(userID);
                    Bitmap bitmap = null;

                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getApplication().getContentResolver(), resultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] data = baos.toByteArray();

                    UploadTask uploadTask = filePath.putBytes(data);

                    Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();

                            }
                            Toast.makeText(CustomerProfileActivity.this, "Image uploaded", Toast.LENGTH_SHORT).show();
                            // Continue with the task to get the download URL
                            return filePath.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            if (task.isSuccessful()) {
                                Uri downloadUri = task.getResult();
                                Map newImage = new HashMap();
                                newImage.put("profileImageUrl", downloadUri.toString());
                                mCustomerDatabase.updateChildren(newImage);
                                finish();
                                return;
                            } else {
                                // Handle failures
                                Toast.makeText(CustomerProfileActivity.this, "Image not uploaded", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });




                }else{
                    finish();
                }

                Intent intent = new Intent(CustomerProfileActivity.this, CustomerMapsActivity.class);
                startActivity(intent);
                finish();


            }
        });

    }

    private void getUserInfo() {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK);
        final Uri imageUri = data.getData();
        resultUri = imageUri;
        mProfileImage.setImageURI(resultUri);
    }
}
