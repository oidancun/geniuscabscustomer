package ke.co.eloise.geniuscabs;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DriverDetailsActivity extends AppCompatActivity {

    private TextView mIdNumber, mVehicleMake, mVehicleModel, mInsuranceProvider, mStatus;
    private Button mConfirm;
    private FirebaseAuth mAuth;
    private DatabaseReference mDriverDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_details);

        mIdNumber = findViewById(R.id.password_et);
        mVehicleMake = findViewById(R.id.password_et1);
        mVehicleModel = findViewById(R.id.password_et2);
        mInsuranceProvider = findViewById(R.id.password_et3);
        mStatus = findViewById(R.id.password_et4);
        mAuth = FirebaseAuth.getInstance();
        mConfirm = (Button) findViewById(R.id.continue_btn);

        final String userId = mAuth.getCurrentUser().getUid();

        DatabaseReference mCustomerDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(userId);
        mCustomerDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.exists() && dataSnapshot.getChildrenCount()>0){
                    Map<String, Object> map = (Map<String, Object>) dataSnapshot.getValue();
                    if(map.get("idnumber") != null){
                        String idNumber = map.get("idnumber").toString();
                        mIdNumber.setText(idNumber);
                    }
                    if(map.get("make") != null){
                        String vehicleMake = map.get("make").toString();
                        mVehicleMake.setText(vehicleMake);
                    }
                    if(map.get("model") != null){
                        String vehicleModel = map.get("model").toString();
                        mVehicleModel.setText(vehicleModel);
                    }
                    if(map.get("provider") != null){
                        String provider = map.get("provider").toString();
                        mInsuranceProvider.setText(provider);
                    }
                    if(map.get("status") != null){
                        String status = map.get("status").toString();
                        mStatus.setText(status);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String idNumber = mIdNumber.getText().toString();
                String vehicleMake = mVehicleMake.getText().toString();
                String vehicleModel = mVehicleModel.getText().toString();
                String provider = mInsuranceProvider.getText().toString();
                String status = mStatus.getText().toString();

                Map userInfo = new HashMap();
                userInfo.put("idnumber", idNumber);
                userInfo.put("make", vehicleMake);
                userInfo.put("model", vehicleModel);
                userInfo.put("provider", provider);
                userInfo.put("status", status);
                mDriverDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Drivers").child(userId);
                mDriverDatabase.updateChildren(userInfo);

                Intent intent = new Intent(DriverDetailsActivity.this, DriverMapsActivity.class);
                startActivity(intent);
                finish();


            }
        });





    }
}
